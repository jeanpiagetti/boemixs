import 'package:boemixs/model/Customer.dart';
import 'package:boemixs/model/Product.dart';
import 'package:boemixs/view/InitialScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class RegisterCustomer extends StatefulWidget {
  const RegisterCustomer({Key? key}) : super(key: key);

  @override
  _RegisterCustomerState createState() => _RegisterCustomerState();
}

class _RegisterCustomerState extends State<RegisterCustomer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Cadastro de Clientes")),
      body: Padding(padding: const EdgeInsets.all(16.0), child: _bodyAppBar()),
    );
  }

  Widget _bodyAppBar() {
    TextEditingController _nameController = TextEditingController();
    TextEditingController _emailController = TextEditingController();
    TextEditingController _phoneController = TextEditingController();
    return Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
      Center(
        child: Text("Formulário de cadastro"),
      ),
      TextField(
        decoration: InputDecoration(labelText: 'Nome'),
        controller: _nameController,
        keyboardType: TextInputType.name,
      ),
      TextField(
        decoration: InputDecoration(labelText: 'Email Cliente'),
        controller: _emailController,
        keyboardType: TextInputType.number,
      ),
      TextField(
        decoration: InputDecoration(labelText: 'Telefone'),
        controller: _phoneController,
        keyboardType: TextInputType.number,
      ),
      const SizedBox(height: 10),
      ElevatedButton(
          onPressed: () {
            final String nameFromField = _nameController.text;
            final String emailField = _emailController.text;
            final String phoneFromField = _phoneController.text;
            final Customer newCustomer = Customer(
                nameFromField, emailField, phoneFromField, Product('', 0, 0));
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => InitialScreen()));
          },
          child: Text("Cadastro"))
    ]);
  }
}
