import 'dart:html';

import 'package:boemixs/model/Product.dart';
import 'package:boemixs/view/ListProduct.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class RegisterProduct extends StatefulWidget {
  const RegisterProduct({Key? key}) : super(key: key);

  @override
  _RegisterProductState createState() => _RegisterProductState();
}

class _RegisterProductState extends State<RegisterProduct> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Cadastro de produtos")),
      body: Padding(padding: const EdgeInsets.all(16.0), child: bodyAppBar()),
    );
  }

  Widget bodyAppBar() {
    TextEditingController _nameController = TextEditingController();
    TextEditingController _amountController = TextEditingController();
    TextEditingController _valueController = TextEditingController();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Center(
          child: Text("Formulário de cadastro"),
        ),
        TextField(
          decoration: InputDecoration(labelText: 'Nome'),
          controller: _nameController,
          keyboardType: TextInputType.name,
        ),
        TextField(
          decoration: InputDecoration(labelText: 'Quantidade'),
          controller: _amountController,
          keyboardType: TextInputType.number,
        ),
        TextField(
          decoration: InputDecoration(labelText: 'Valor'),
          controller: _valueController,
          keyboardType: TextInputType.number,
        ),
        const SizedBox(height: 20),
        ElevatedButton(
            onPressed: () {
              final String nameFromField = _nameController.text;
              final int amountFromField = int.parse(_amountController.text);
              final double valueFromField = double.parse(_valueController.text);
              final Product newProduct =
                  Product(nameFromField, amountFromField, valueFromField);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ListProduct(product: newProduct)));
            },
            child: Text("Cadastro"))
      ],
    );
  }
}
