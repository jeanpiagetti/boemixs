import 'package:boemixs/view/RegisterCustomer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:boemixs/view/RegisterProduct.dart';

class InitialScreen extends StatelessWidget {
  const InitialScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ElevatedButton(
              onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RegisterProduct()))
                  },
              child: Text("Produtos", style: TextStyle(fontSize: 20))),
          const SizedBox(height: 20),
          ElevatedButton(
              onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RegisterCustomer()))
                  },
              child: Text("Clientes", style: TextStyle(fontSize: 20)))
        ],
      ),
    );
  }
}
