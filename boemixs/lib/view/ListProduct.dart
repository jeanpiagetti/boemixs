import 'package:boemixs/model/Product.dart';
import 'package:flutter/material.dart';

class ListProduct extends StatelessWidget {
  final Product product;
  const ListProduct({Key? key, required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Um titulo qualquer',
      home: Scaffold(
        appBar: AppBar(title: Text('Titulo')),
        body: _bodyTable(this.product),
      ),
    );
  }
}

@override
Widget _bodyTable(Product product) {
  return Table(
    border: TableBorder.all(),
    columnWidths: const <int, TableColumnWidth>{
      0: FlexColumnWidth(),
      1: FlexColumnWidth(),
      2: FlexColumnWidth(),
    },
    defaultVerticalAlignment: TableCellVerticalAlignment.middle,
    children: <TableRow>[
      TableRow(
        children: <Widget>[
          TableCell(
            verticalAlignment: TableCellVerticalAlignment.top,
            child: Container(
              height: 32,
              child: Center(
                child: Text('Nome', style: TextStyle(fontSize: 20)),
              ),
            ),
          ),
          TableCell(
            verticalAlignment: TableCellVerticalAlignment.top,
            child: Container(
              height: 32,
              child: Center(
                child: Text('Quantidade', style: TextStyle(fontSize: 20)),
              ),
            ),
          ),
          TableCell(
            verticalAlignment: TableCellVerticalAlignment.top,
            child: Container(
              height: 32,
              child: Center(
                child: Text('Valor', style: TextStyle(fontSize: 20)),
              ),
            ),
          ),
        ],
      ),
      TableRow(
        decoration: const BoxDecoration(
          color: Colors.grey,
        ),
        children: <Widget>[
          Container(
            width: 128,
            child: Text(product.name),
          ),
          Container(
            width: 32,
            color: Colors.grey,
            child: Text('${product.amount.toString()}un.'),
          ),
          Center(
            child: Container(
              width: 32,
              color: Colors.grey,
              child: Text('R\$ ${product.value.toString()}'),
            ),
          ),
        ],
      ),
    ],
  );
}
