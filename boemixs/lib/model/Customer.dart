import 'package:boemixs/model/Product.dart';

class Customer {
  final String nameCustomer;
  final String emailCustomer;
  final String phone;
  final Product product;

  Customer(this.nameCustomer, this.emailCustomer, this.phone,this.product);
}
